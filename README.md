# What is this?

This is a newbie attempt at making an amplified audio circuit that can output to a 4-8 Ohm speakek and also flash LEDs based on high freequency and low freequency signals. 

In short, its an analog LED VU like meter. It is not an actual Vu meter as there are no reference levels. 

## How does it work? 

This works by using an LM386-N TI Chip (OpAmp) to amplify an audio signal at 200 Gain with bass boost (optional) to a speaker output, a passive high pass filter and passive low pass filter to flash a series of LEDs. The high and low pass filters essentially separating the low frequency signals from the high frequency. So one set of LEDs flash on bass and the other flash on treble. 

The speaker should be 4 to 8 Ohms. Do not use on lower Ohm speakers as it can damage the speaker. I think higher ohm speakers simply will not be driven by 9 or 12 Volts that the LM386-N can handle. I think the higher Ohm speakers could also damage the Amp, but I have not tested because I do not have a speaker greater than 8 Ohms. 

The 200 gain setting is optional and so is the bass boost. Please see the LM3860-N TI datasheets for the example circuit diagrams. 

The LEDs used in this project were simple THT 5MM LEDs.

The input signal I used was my Laptop Aux (Headphone) out to a 2 Channel converter RCA (without video). The white and red connectors alligator clipped to speaker input and the sleave alligator clicked to ground. The inverted input (negative input) of the OpAmp goes to ground too.  

![Schematic](schematic.PNG)


## Warning and Todo:

Always be careful whem messing around with electronics. This is a 9 Volt circuit but can still be dangerous.

Please note that my schematic may have mistakes. I suspect that the high and low pass filters could be labeled backwards and some of the traces could be backwards. This is my first complex schematic create with a breadboard full of jumper wires. It is a lot like looking at code with too many if statements, or a really complex one liner.

Todo: 
- [] Clean up the breadboard or get the circuit onto protoboard. 
- [] Upload simple gif of operation. 
- [] Check the schematic for component and trace correctness, again. 
- [] Check if different N-Type transitors can be used. 

